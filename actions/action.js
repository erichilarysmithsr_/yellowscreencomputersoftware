"use strict";
let datafire = require('datafire');

let stackexchange = require('@datafire/stackexchange').actions;
module.exports = new datafire.Action({
  handler: async (input, context) => {
    let users = await Promise.all([].map(item => stackexchange.users.get({
      site: "",
    }, context)));
    return users;
  },
});
